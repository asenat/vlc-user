
Contact
===============

**User Support**

See our :doc:`user support pages </support/gethelp>` for VLC or other VideoLAN software. 

**Administrative Email**

To contact us by e-mail, send a mail to ``videolan at videolan dot org`` (no support provided here) and write in either French, German or English.

**Regular Mail**

If you need to send a postal mail to us, kindly mail it to the address below;

    +-----------------+
    | Postal Address  |
    +=================+
    | VideoLAN        |
    +-----------------+
    | 18, rue Charcot |
    +-----------------+
    | 75013 Paris     |
    +-----------------+
    | France          |
    +-----------------+