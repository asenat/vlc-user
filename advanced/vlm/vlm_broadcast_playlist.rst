.. _vlm-playlist:

##################################
VLM Example: Broadcast A Playlist
##################################

This page describes how to broadcast a playlist using a VLM. To get a basic idea of what VLM is and how to broadcast a simple stream using a ``.vlm`` file, refer to :ref:`VLM Introduction <vlm-introduction>`.

Broadcasting multiple media files successively can be done by specifying multiple ``input`` commands for the same channel. VLM simply executes the next input command when the streaming from the earlier one ends. 

For this example, we will stream three songs ``one.mp3``, ``two.mp3``, and ``three.mp3`` over HTTP. Hence, refer to Stream over HTTP to get a basic idea of how streaming over HTTP works. 

**************************
Configuring the VLM file
**************************

Consider the following code:

.. code-block ::

   new channel_1 broadcast enabled
   setup channel_1 input "file:///C:/Users/User_name/Desktop/one.mp3"
   setup channel_1 input "file:///C:/Users/User_name/Desktop/two.mp3"
   setup channel_1 input "file:///C:/Users/User_name/Desktop/three.mp3"
   setup channel_1 output #http{mux=ts,dst=:8090/stream}
   setup channel_1 option sout-keep
   setup channel_1 option file-caching=1000
   control channel_1 play

We have used ``sout-keep`` so that the streaming doesn't break when a song ends.

For the above code:

 + Create a new file in any text editor and copy the code.
 + Modify the file paths according to where the files are located in your system.
 + Save the file as ``playlist.vlm``.


************************
Start the broadcast
************************

Whenever you wish to broadcast the playlist, open the terminal and navigate to where you have saved ``playlist.vlm``. Then run the following command:

.. code-block ::

   $ vlc --vlm-conf=playlist.vlm


***************************
Receiving the broadcast
***************************

The receiving devices can receive the stream as they would receive a normal HTTP stream. As HTTP streams are published at the IP address of the streaming device, we need to specify the IP address of the streaming device on the recieving device(s) to connect. 

Assuming that the IP address of the streaming device is ``192.168.0.101``, run the following command on the receiving device(s) to connect and start playing the playlist:

.. code-block ::

   $ vlc http://192.168.0.101:8090/stream

Following is how the network might look: 

.. graphviz::

    digraph http {
      rankdir=TB;
      node [shape=circle];
      
      { node [width=0 shape=point label=""]; idle;
      }
      
      { node [shape=plaintext]; "$ vlc --vlm-conf=playlist.vlm", "$ vlc http://192.168.0.101:8090/stream";
      }
      
      
      "Receiving Device \n IP: 192.168.0.102" -> "Streaming Device \n IP: 192.168.0.101" [label="The receiving device requests for\nthe playlist (HTTP stream)"]

      "Streaming Device \n IP: 192.168.0.101" -> "Receiving Device \n IP: 192.168.0.102" [label="The server then sends\nthe stream to the client"]
      


      "Streaming Device \n IP: 192.168.0.101" -> "$ vlc --vlm-conf=playlist.vlm" [style=invis]
      "Receiving Device \n IP: 192.168.0.102" -> "$ vlc http://192.168.0.101:8090/stream" [style=invis]
      

     subgraph devices {
      rank=same;
      "Streaming Device \n IP: 192.168.0.101", "Receiving Device \n IP: 192.168.0.102";
      }
      

    }