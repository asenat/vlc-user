######################################################
Use FFmpeg AMF encoders for transcoding and streaming
######################################################

VLC media player supports hardware-accelerated AMF encoders by using the FFmpeg library.

**************************
Enable AMF in VLC 3.X
**************************

The easiest way to use AMF encoders in VLC is to download the latest `nightly builds <https://nightlies.videolan.org>`_ or compile VLC from sources.

Since VLC 4.0 nightly builds might be unstable, we recommend to use the stable VLC version from branch 3.0.18.
AMF is not backported to VLC 3.x version yet, so to you might need to backport this feature by yourself.

.. code-block ::

   git clone http://git.videolan.org/git/vlc.git vlc
   cd vlc
   git checkout 3.0.18
   # The commits below are needed for enabling AMF support in VLC
   git chery-pick f1a2665f609d9d5bd7e662a8a2519018e1d3f61c
   git chery-pick dc1decb2609ca44c78b8bd2db77dd87a85dbcb92

.. warning::

   If you are compiling VLC from sources, please make sure that you have compiled the FFmpeg library with AMF support (check the build logs from FFmpeg to confirm).

*****************************
Using AMF Encoders in VLC
*****************************

#. Run VLC and navigate to the FFmpeg menu for video codecs: :menuselection:`Tools --> Preferences --> Show settings (All) --> Input/Codecs --> Video Codecs --> FFmpeg`

   .. figure::  /images/advanced/transcode/transcode_ffmpeg_amf_all_settings.png

#. Enter the `h264_amf` or `hevc_amf` encoder as the codec name inside the Encoding section.

   .. figure::  /images/advanced/transcode/transcode_ffmpeg_amf_encoder_settings.png

#. By default, VLC uses the x264/x265 software encoders because they have a higher priority than the FFmpeg encoders in the VLC settings. To force using `h264_amf` / `hevc_amf` hardware encoders instead of x264/x265, you need to choose `FFmpeg audio/video encoder` in the stream output menu:

   :menuselection:`Tools --> Preferences --> Stream Output --> Sout stream --> Transcode section`

   .. figure::  /images/advanced/transcode/transcode_ffmpeg_amf_transcode_settings.png

#. AMF encoders can be used in VLC for transcoding and streaming. In VLC, transcoding is exactly the same as streaming across the network, except that the output is sent to a file instead of the network.

   .. note::

      You can refer to the following guides on how to transcode and stream video using VLC:

      * :ref:`Introduction to Transcoding <introduction-to-transcoding>`
      * :ref:`Streaming with VLC <streaming-with-vlc>`

#. To check that the AMF encoders work, you may increase the logging level in the messages menu:

   :menuselection:`Tools --> Messages`

   Choose ``Verbosity level 2 (debug)``. If the AMF encoder works, then during transcoding/streaming there will be something like this inside the VLC logs:

   .. code-block ::

       avcodec debug: using FFmpeg Lavc58.134.100
       ....
       avcodec debug: found encoder H264 - MPEG-4 AVC (part 10)
       main debug: using encoder module "avcodec"


#. If you need to pass options to the AMF encoders, then you can use the ``Advanced options`` field in the FFmpeg settings:

   :menuselection:`Tools --> Preferences --> Show settings (All) --> Input/Codecs --> Video Codecs -->  FFmpeg`

   The full list of options for the AMF encoder can be seen with the following command:

   .. code-block ::

      ffmpeg -h encoder=h264_amf

   Examples of passing options to the AMF encoder in FFmpeg:

   .. code-block ::

      # transcode using the AMF H264 HW encoder and default encoding settings
      ffmpeg -i input.mp4 -codec:v h264_amf output.mp4

      # set the bitrate to 5000k
      ffmpeg -i input.mp4 -codec:v h264_amf -b:v 5000k output.mp4

      # use AMF HEVC HW encoder with the latency constrained variable bitrate method
      # for rate control, pre-analysis enabled, and AMF logging to the debug output
      ffmpeg -i input.mp4 -codec:v hevc_amf -rc 1 -preanalysis true -log_to_dbg true output.mp4

      # set the encoding usage to "ultra low latency" as the baseline template for encoder settings
      ffmpeg -i input.mp4 -codec:v hevc_amf -usage ultralowlatency output.mp4


   .. figure::  /images/advanced/transcode/transcode_ffmpeg_amf_advanced_options.png

#. If you prefer command line interface for transcoding/streaming, then you can use `--sout` command for passing AMF options arguments

   .. code-block ::

      $ vlc sample.mp4 --sout-avcodec-codec=hevc_amf --sout-avcodec-options="quality=speed, rc=1" --sout="#transcode{vcodec=hevc, venc=avcodec, fps=30, acodec=none, scodec=none}:std{access=file, mux=mkv, dst=sample_new.mkv}"

*******************
External References
*******************

More information about AMF encoder settings can be found in the AMF documentation:

* `AMF h.264 Video Encoder Programming Guide <https://github.com/GPUOpen-LibrariesAndSDKs/AMF/blob/master/amf/doc/AMF_Video_Encode_API.pdf>`_
* `AMF HEVC Video Encoder Programming Guide <https://github.com/GPUOpen-LibrariesAndSDKs/AMF/blob/master/amf/doc/AMF_Video_Encode_HEVC_API.pdf>`_
