###############
 Unusual Cases
###############

.. toctree::
   :maxdepth: 2
   
   shut_down_computer.rst
   transcription_linux.rst
   transcoding_with_ffmpeg_amf_codecs.rst
   uncommon.rst

