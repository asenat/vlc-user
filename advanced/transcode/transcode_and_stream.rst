.. _transcode-and-stream:

######################################################
How to transcode a file and stream it simultaneously?
######################################################

This page describes how to "transcode on the fly", which is transcoding and streaming simultaneously. As an example, we will transcode an ``MP4`` file to an ``MKV`` container by encoding it with ``h265`` and ``mpga`` codecs, and stream the output over HTTP.

Because the sout chain works as a pipeline, we can stream while transcoding by simply combining the code of: 

 + Transcoding a file, and 
 + Streaming over HTTP.

Hence, as a pre-requisite, refer to :ref:`How to transcode a file <transcode-a-file>` to understand how to perform basic transcoding, and refer to Stream over HTTP to understand how to stream a file over HTTP in the local network.


****************************
How to send the stream?
****************************

In order to send the transcoded stream, we will do the following step-by-step: 

 + Transcode the MP4 file by re-encding it with the ``h265`` and the ``mpga`` codecs, 
 + Mux the transcoded streams into an ``MKV`` container, and 
 + Stream the final output over HTTP at port number ``8090`` and path name ``streaming`` (you can choose any port number and path name).

Steps 1 an 2 can be implemented by using the ``transcode`` and the ``std`` blocks as in the following code snippet:

.. code-block ::

   transcode{vcodec=h265, acodec=mpga}:std{mux=mkv}

While step 3 can be implemented by setting the ``access`` and ``dst`` parameters of the ``std`` block in the following manner:

.. code-block ::

   std{access=http, dst=:8090/streaming}

By combining both of these parts we get the code that should be executed on the sending device to start the transcoding and streaming:

.. code-block ::

   $ vlc sample.mp4 --sout="#transcode{vcodec=h265, acodec=mpga}:std{mux=mkv, access=http, dst=:8090/streaming}"


***********************************
How to receive the stream?
***********************************

As HTTP is a pull protocol, the stream will be published over the IP address of the sending device at the specified port number and path (which are ``8090`` and ``streaming`` in our example).

Hence, we need the IP address of the streaming device in order to catch the stream. Suppose the IP address of the streaming device is ``192.168.0.101``. 

Run the following code on the receiving device(s) to catch the transcoded stream:

.. code-block ::

   $ vlc http://192.168.0.101:8090/streaming

As soon as the above code is run, a VLC Media Player window will open and start playing the stream.

Graphically, this is how the network might look:

.. graphviz::

	digraph http {
	    rankdir=TB;
	    node [shape=circle];
	    
	    { node [width=0 shape=point label=""]; idle;
	    }
	    
	    { node [shape=plaintext]; "$ vlc sample.mp4 \n--sout=\"#transcode{vcodec=h265, acodec=mpga}:\nstd{mux=mkv, access=http, dst=:8090/streaming}\"",  "$ vlc http://192.168.0.101:8090/streaming";
	    }
	    
	    
	    "Receiving Device \n IP: 192.168.0.102" -> "Streaming Device \n IP: 192.168.0.101" [label="The receiving device requests for\nthe transcoded stream"]

	    "Streaming Device \n IP: 192.168.0.101" -> "Receiving Device \n IP: 192.168.0.102" [label="The server then sends\nthe stream to the client"] 
	    


	    "Streaming Device \n IP: 192.168.0.101" -> "$ vlc sample.mp4 \n--sout=\"#transcode{vcodec=h265, acodec=mpga}:\nstd{mux=mkv, access=http, dst=:8090/streaming}\"" [style=invis]
	    "Receiving Device \n IP: 192.168.0.102" -> "$ vlc http://192.168.0.101:8090/streaming" [style=invis]
	    

	   subgraph devices {
	    rank=same;
	    "Streaming Device \n IP: 192.168.0.101", "Receiving Device \n IP: 192.168.0.102";
	    }
	    

	  }

|